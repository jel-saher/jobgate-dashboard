import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { LayoutComponent } from './components/layout/layout.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { LoginComponent } from './components/login/login.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './components/register/register.component';
import { EntreprisesComponent } from './components/entreprises/entreprises.component';
import { OffresComponent } from './components/offres/offres.component';
import { CandidatComponent } from './components/candidat/candidat.component';
import { RecherchePipe } from './pipes/recherche.pipe';
import { SpecialityComponent } from './components/speciality/speciality.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { PlacesComponent } from './components/places/places.component';
import { JobsComponent } from './components/jobs/jobs.component';
import { RecherchejobPipe } from './pipes/recherchejob.pipe';
import { CandidatureComponent } from './components/candidature/candidature.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    SidebarComponent,
    LayoutComponent,
    FooterComponent,
    HomeComponent,
    LoginComponent,
    RegisterComponent,
    EntreprisesComponent,
    OffresComponent,
    CandidatComponent,
    RecherchePipe,
    SpecialityComponent,
    PlacesComponent,
    JobsComponent,
    RecherchejobPipe,
    CandidatureComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
