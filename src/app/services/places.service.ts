import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PlacesService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallplaces() {
    return this.http.get(`${environment.baseurl}/place/listplace`, {
      headers: this.headersoption,
    });
  }

  getbyidplaces(id: any) {
    return this.http.get(`${environment.baseurl}/place/byidplace/${id}`, {
      headers: this.headersoption,
    });
  }

  deleteplaces(id: any) {
    return this.http.delete(`${environment.baseurl}/place/deleteplace/${id}`, {
      headers: this.headersoption,
    });
  }

  updateplaces(id: any, newplace: any) {
    return this.http.put(
      `${environment.baseurl}/place/updateplace/${id}`,
      newplace,
      {
        headers: this.headersoption,
      }
    );
  }
  createplaces(place: any) {
    return this.http.post(`${environment.baseurl}/place/createplace`, place, {
      headers: this.headersoption,
    });
  }
}
