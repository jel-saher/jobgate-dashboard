import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SpecialityService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  getallspeciality() {
    return this.http.get(`${environment.baseurl}/speciality/listspeciality`);
  }

  getbyidspeciality(id: any) {
    return this.http.get(
      `${environment.baseurl}/speciality/byidspeciality/${id}`
    );
  }

  deletespeciality(id: any) {
    return this.http.delete(
      `${environment.baseurl}/speciality/deletespeciality/${id}`,
      {
        headers: this.headersoption,
      }
    );
  }

  updatespeciality(id: any, newspeciality: any) {
    return this.http.put(
      `${environment.baseurl}/speciality/updatespeciality/${id}`,
      newspeciality,
      {
        headers: this.headersoption,
      }
    );
  }
  createspeciality(speciality: any) {
    return this.http.post(
      `${environment.baseurl}/speciality/createspeciality`,
      speciality,{
        headers: this.headersoption,
      }
    );
  }
}
