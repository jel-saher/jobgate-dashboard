import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class PasswordService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });
  forgetPassword(email: any) {
    return this.http.post(`${environment.baseurl}/forgetpassword`, email);
  }
  changePassword(newpwd: any) {
    return this.http.put(`${environment.baseurl}/changepassword`, newpwd, {
      headers: this.headersoption,
    });
  }
  resetPassword(newp:any) {
    return this.http.post(`${environment.baseurl}/resetpassword`,newp);
  }
}
