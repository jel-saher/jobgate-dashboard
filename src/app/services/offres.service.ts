import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class OffresService {
  constructor(private http: HttpClient) {}
  token = localStorage.getItem('token')!;
  headersoption = new HttpHeaders({
    Authorization: 'Bearer ' + this.token,
  });

  getalloffre() {
    return this.http.get(`${environment.baseurl}/offre/listoffre`);
  }
  getoneoffre(id: any) {
    return this.http.get(`${environment.baseurl}/offre/byidoffre/${id}`);
  }
  deleteoffre(id: any) {
    return this.http.delete(`${environment.baseurl}/offre/deleteoffre/${id}`);
  }

  confirmoffre(id: any, newoffre: any) {
    return this.http.put(
      `${environment.baseurl}/confirmedoffre/${id}`,
      newoffre,
      {
        headers: this.headersoption,
      }
    );
  }
}
