import { registerLocaleData } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CandidatComponent } from './components/candidat/candidat.component';
import { CandidatureComponent } from './components/candidature/candidature.component';
import { EntreprisesComponent } from './components/entreprises/entreprises.component';
import { ForgotpasswordComponent } from './components/forgotpassword/forgotpassword.component';
import { HomeComponent } from './components/home/home.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginComponent } from './components/login/login.component';
import { OffresComponent } from './components/offres/offres.component';
import { PlacesComponent } from './components/places/places.component';
import { RegisterComponent } from './components/register/register.component';
import { ResetpasswordComponent } from './components/resetpassword/resetpassword.component';
import { SpecialityComponent } from './components/speciality/speciality.component';
import { AuthGuard } from './guards/auth.guard';

const routes: Routes = [
  { path: '', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  {path:'forgotpassword',component:ForgotpasswordComponent},
  {path:'resetpassword/:id',component:ResetpasswordComponent},
  {
    path: 'home',
    canActivate: [AuthGuard],
    component: HomeComponent,
    children: [
      { path: '', component: OffresComponent },
      { path: 'entreprises', component: EntreprisesComponent },
      { path: 'candidat', component: CandidatComponent },
      { path: 'speciality', component: SpecialityComponent },
      { path: 'places', component: PlacesComponent },
      { path: 'offre', component: OffresComponent },
      { path: 'candidature', component: CandidatureComponent },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
