import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'recherchejob',
})
export class RecherchejobPipe implements PipeTransform {
  transform(value: any, term: any): any {
    if (term == null) {
      return value;
    } else {
      return value.filter((item: any) =>
        item.titre.toLowerCase().includes(term.toLowerCase())
      );
    }
  }
}
