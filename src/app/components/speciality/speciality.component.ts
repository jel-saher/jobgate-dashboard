import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SpecialityService } from 'src/app/services/speciality.service';
import Swal from 'sweetalert2';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-speciality',
  templateUrl: './speciality.component.html',
  styleUrls: ['./speciality.component.css'],
})
export class SpecialityComponent implements OnInit {
  listspeciality: any;
  specialityForm: FormGroup;
  specialForm: FormGroup;
  closeResult = '';
  submitted = false;
  constructor(
    private specialityservice: SpecialityService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.specialityForm = this.formBuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.specialForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.getallspeciality();
  }

  get f() {
    return this.specialForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.specialForm.invalid) {
      return;
    }

    this.specialityservice
      .createspeciality(this.specialForm.value)
      .subscribe((res: any) => {});

    Swal.fire('Created', 'Your Speciality has been created.', 'success');
    this.onReset();

    // display form values on success
    /* alert(
      'SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4)
    ); */
  }
  onReset() {
    this.submitted = false;
    this.specialForm.reset();
    this.getallspeciality();
  }

  getallspeciality() {
    this.specialityservice.getallspeciality().subscribe((res: any) => {
      this.listspeciality = res['data'];
    });
  }

  deletespeciality(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.specialityservice.deletespeciality(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your Speciality has been deleted.', 'success');
          this.getallspeciality();
        });
      }
    });
  }

  open(content: any, i: any) {
    this.specialityForm.patchValue({
      _id: i._id,
      name: i.name,

      description: i.description,
    });

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updatespeciality() {
    this.specialityservice
      .updatespeciality(
        this.specialityForm.value._id,
        this.specialityForm.value
      )
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Speciality Updated');
        this.getallspeciality();
      });
  }
}
