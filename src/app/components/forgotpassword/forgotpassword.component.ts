import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PasswordService } from 'src/app/services/password.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.css'],
})
export class ForgotpasswordComponent implements OnInit {
  ForgetForm: FormGroup;
  email: any;
  constructor(
    private passwordservice: PasswordService,
    private formbuilder: FormBuilder,
    private route: Router
  ) {}
  ngOnInit(): void {
    this.ForgetForm = this.formbuilder.group({
      email: ['', Validators.required],
    });
  }
  Forgetpassword() {
    return this.passwordservice
      .forgetPassword(this.ForgetForm.value)
      .subscribe((res: any) => {
        Swal.fire(
          'Un lien est envoyé par mail veuillez verifier votre email.',
          'success'
        );
        console.log('ok');
        this.route.navigateByUrl('/resetpassword');
      });
  }
}
