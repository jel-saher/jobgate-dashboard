import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { PasswordService } from 'src/app/services/password.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.css'],
})
export class ResetpasswordComponent implements OnInit {
  resetForm: FormGroup;
  d: any;
  id = this.activateroute.snapshot.params['id'];

  constructor(
    private passwordservice: PasswordService,
    private formBuilder: FormBuilder,
    private activateroute: ActivatedRoute,
    private route: Router
  ) {}

  ngOnInit(): void {
    this.resetForm = this.formBuilder.group({
      newpassword: ['', Validators.required],
      resetpassword: [''],
    });
    console.log('id : ', this.id);
  }

  resetPassword() {
    this.resetForm.patchValue({
      resetpassword: this.id,
    });
    this.passwordservice
      .resetPassword(this.resetForm.value)
      .subscribe((res: any) => {
        Swal.fire('OK.', 'success');
        this.route.navigateByUrl('/');
      });
  }
}
