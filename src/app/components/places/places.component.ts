import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalDismissReasons, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PlacesService } from 'src/app/services/places.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-places',
  templateUrl: './places.component.html',
  styleUrls: ['./places.component.css'],
})
export class PlacesComponent implements OnInit {
  listplaces: any;
  placesForm: FormGroup;
  plForm: FormGroup;
  closeResult = '';
  submitted = false;

  constructor(
    private placeservice: PlacesService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal
  ) {}

  ngOnInit(): void {
    this.placesForm = this.formBuilder.group({
      _id: ['', Validators.required],
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.plForm = this.formBuilder.group({
      name: ['', Validators.required],
      description: ['', Validators.required],
    });
    this.getallplaces();
  }

  getallplaces() {
    this.placeservice.getallplaces().subscribe((res: any) => {
      this.listplaces = res['data'];
    });
  }

  get f() {
    return this.plForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.plForm.invalid) {
      return;
    }

    this.placeservice
      .createplaces(this.plForm.value)
      .subscribe((res: any) => {});

    Swal.fire('Created', 'Your Place has been created.', 'success');
    this.onReset();

    // display form values on success
    /* alert(
      'SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4)
    ); */
  }
  onReset() {
    this.submitted = false;
    this.plForm.reset();
    this.getallplaces();
  }

  deleteplaces(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.placeservice.deleteplaces(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your Place has been deleted.', 'success');
          this.getallplaces();
        });
      }
    });
  }

  open(content: any, i: any) {
    this.placesForm.patchValue({
      _id: i._id,
      name: i.name,
      description: i.description,
    });

    this.modalService
      .open(content, { ariaLabelledBy: 'modal-basic-title' })
      .result.then(
        (result) => {
          this.closeResult = `Closed with: ${result}`;
        },
        (reason) => {
          this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
        }
      );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  updateplaces() {
    this.placeservice
      .updateplaces(this.placesForm.value._id, this.placesForm.value)
      .subscribe((res: any) => {
        console.log(res);
        Swal.fire('Place Updated');
        this.getallplaces();
      });
  }
}
