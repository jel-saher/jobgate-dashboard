import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, RouterLinkActive } from '@angular/router';
import { OffresService } from 'src/app/services/offres.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-offres',
  templateUrl: './offres.component.html',
  styleUrls: ['./offres.component.css'],
})
export class OffresComponent implements OnInit {
  listoffre: any;
  id = this.activateroute.snapshot.params['id'];
  byid: any;
  searchname: any;
  nbroffre: any;
  listof: any;
  constructor(
    private offreservice: OffresService,
    private activateroute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getalloffre();
  }

  getalloffre() {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'];
      console.log('liste offre :', this.listoffre);
      this.nbroffre = this.listoffre.length;
      this.getoneoffre(this.listoffre[0]._id);
    });
  }

  getoneoffre(id: any) {
    this.offreservice.getoneoffre(id).subscribe((res: any) => {
      this.byid = res['data'];
      console.log('Detail :  ', this.byid);
    });
  }

  deleteoffre(id:any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.offreservice.deleteoffre(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your Offre has been deleted.', 'success');
          this.getalloffre();
        });
      }
    });
  }

  confirmoffre(id: any, newuser: any) {
    this.offreservice.confirmoffre(id, newuser).subscribe((res: any) => {
      console.log(res);
      this.getalloffre();
    });
  }

  OnChangeType(event: any) {
    this.offreservice.getalloffre().subscribe((res: any) => {
      this.listoffre = res['data'].filter(
        (element: any) => element.type == event.target.value
      );
      console.log('list offre type :', this.listoffre);
    });
  }
}
