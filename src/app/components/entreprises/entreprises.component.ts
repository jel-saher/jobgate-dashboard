import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { RegisterService } from 'src/app/services/register.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-entreprises',
  templateUrl: './entreprises.component.html',
  styleUrls: ['./entreprises.component.css'],
})
export class EntreprisesComponent implements OnInit {
  searchname: any;
  id = this.activateroute.snapshot.params['id'];
  listuser: any;
  byid: any;
  nbrcompanies: any;
  constructor(
    private registerservice: RegisterService,
    private activateroute: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.getalluser();
    // this.isconfirmed();
  }

  getalluser() {
    this.registerservice.getalluser().subscribe((res: any) => {
      this.listuser = res['data'].filter(
        (element: any) => element.role == 'entreprise'
      );

      console.log('liste entreprise :', this.listuser);
      this.nbrcompanies = this.listuser.length;

      // console.log('aaaaaaaaaa: ', this.listuser[1]._id);
      this.getoneuser(this.listuser[0]._id);
    });
  }

  /* isconfirmed(){
   console.log ("aaaaaaa",this.listuser[1].confirmed);
  } */
  getoneuser(id: any) {
    this.registerservice.getbyid(id).subscribe((res: any) => {
      this.byid = res['data'];
      console.log('Detail :  ', this.byid);
    });
  }

  confirmuser(id: any, newuser: any) {
    this.registerservice.confirmuser(id, newuser).subscribe((res: any) => {
      console.log(res);
      this.getalluser();
    });
  }

  deleteuser(id: any) {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.registerservice.deleteuser(id).subscribe((res: any) => {
          Swal.fire('Deleted!', 'Your User has been deleted.', 'success');
          this.getalluser();
        });
      }
    });
  }
}
